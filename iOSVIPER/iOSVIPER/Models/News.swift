//
//  News.swift
//  iOSVIPER
//
//  Created by Sergey Stelmakh on 01.11.2021.
//

import Foundation

struct News: Decodable {
    var userId: Int
    var id: Int
    var title: String
    var body: String
}
