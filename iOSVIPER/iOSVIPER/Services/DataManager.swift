//
//  DataManager.swift
//  iOSVIPER
//
//  Created by Sergey Stelmakh on 02.11.2021.
//

import Foundation

class DataManager {
    static let shared = DataManager()
    
    private var news = [News]()
    
    
    func setNews(_ news: [News]) {
        self.news = news
    }
    
    func getNew(at indexPath: IndexPath) -> News {
        return news[indexPath.row]
    }
}
