//
//  NetworkManager.swift
//  iOSVIPER
//
//  Created by Sergey Stelmakh on 01.11.2021.
//

import Foundation

class NetworkManager {
    
    static let shared = NetworkManager()
    
    private let api =  "https://jsonplaceholder.typicode.com/posts"
    
    private init() {}
    
    func fetch(completion: @escaping (_ news: [News]) -> Void) {
        guard let url = URL(string: api) else { return }
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else {
                print(error?.localizedDescription ?? "No Discription")
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let news = try decoder.decode([News].self, from: data)
                DataManager.shared.setNews(news)
                DispatchQueue.main.async {
                    completion(news)
                }
            } catch let error {
                print("Error json", error)
            }
        }.resume()
    }
}
