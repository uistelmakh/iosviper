//
//  NewsRouter.swift
//  iOSVIPER
//
//  Created by Sergey Stelmakh on 31.10.2021.
//

import Foundation
import UIKit

/// Протокол роутинга
protocol NewsRouterInputProtocol {
    init(viewController: NewsViewController)
    func openNewsDetailsViewController(with news: News)
}

/// Роутер VIPER-модуля
class NewsRouter: NewsRouterInputProtocol {
    
    weak var viewController: NewsViewController?
    
    required init(viewController: NewsViewController) {
        self.viewController = viewController
    }
    
    
    
    func openNewsDetailsViewController(with news: News) {
        let newsDetailsViewController = NewsDetailsViewController()
    }
}
