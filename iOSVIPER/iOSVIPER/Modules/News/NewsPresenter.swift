//
//  NewsPresenter.swift
//  iOSVIPER
//
//  Created by Sergey Stelmakh on 31.10.2021.
//

import Foundation



/// Протокол для работы Presenter с NewsViewController
protocol NewsViewOutputProtocol {
    init(view: NewsViewInputProtocol)
    func viewDidLoad()
    func didTapCell(at indexPath: IndexPath)
}

class NewsPresenter: NewsViewOutputProtocol {
    
    weak var view: NewsViewInputProtocol?
    var interactor: NewsInteractorInputProtocol?
    var router: NewsRouterInputProtocol?
    
    required init(view: NewsViewInputProtocol) {
        self.view = view
    }
    
    /// подготовка данных
    func viewDidLoad() {
        interactor?.fetchNews()
    }
    
    func didTapCell(at indexPath: IndexPath) {
        interactor?.getNews(at: indexPath)
    }
}

extension NewsPresenter: NewsInteractorOutputProtocol{
    
    func newsDidReceive(_ news: [News]) {
        view?.display(news)
    }
    
    func newDidRecive(_ new: News) {
        router?.openNewsDetailsViewController(with: new)
    }
}

