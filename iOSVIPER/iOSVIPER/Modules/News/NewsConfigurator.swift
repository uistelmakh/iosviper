//
//  NewsConfigurator.swift
//  iOSVIPER
//
//  Created by Sergey Stelmakh on 31.10.2021.
//

import Foundation

protocol NewsConfiguratorInputProtocol {
    func configure(with view: NewsViewController)
}

class NewsConfigurator: NewsConfiguratorInputProtocol {
    func configure(with view: NewsViewController) {
        let presenter = NewsPresenter(view: view)
        let interactor = NewsInteractor(presenter: presenter)
        let router = NewsRouter(viewController: view)
        
        view.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
    }
}

