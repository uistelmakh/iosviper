//
//  ViewController.swift
//  iOSVIPER
//
//  Created by Sergey Stelmakh on 31.10.2021.
//

import UIKit

/// Протокол отображения NewsViewController-а
protocol NewsViewInputProtocol: AnyObject {
    func display(_ news: [News])
}

class NewsViewController: UIViewController {
    
    /// Таблица для отображения новостей
    var tableView = UITableView()
    
    /// константа индентификатора таблицы
    let identifier = "cell"
    private var news = [News]()
    
    private let configurator: NewsConfiguratorInputProtocol = NewsConfigurator()
    
    ///  
    var presenter: NewsViewOutputProtocol?
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(with: self)
        presenter?.viewDidLoad()
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: identifier)
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)
    }
    
    override func viewDidLayoutSubviews() {
        tableView.frame = view.bounds
    }

}

// MARK: - UITableViewDataSource
extension NewsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return news.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        let new = news[indexPath.row]
        cell.textLabel?.text = new.title
        return cell
    }
}

// MARK: - UITableViewDelegate
extension NewsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        //presenter?.didTapCell(at: indexPath.row)
    }
}

// MARK: - NewsViewInputProtocol
extension NewsViewController: NewsViewInputProtocol {
    func display(_ news: [News]) {
        self.news = news
        self.tableView.reloadData()
    }
    
}

