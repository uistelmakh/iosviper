//
//  NewsInteractor.swift
//  iOSVIPER
//
//  Created by Sergey Stelmakh on 31.10.2021.
//

import Foundation

/// Протокол для работы с NewsInteractor
protocol NewsInteractorInputProtocol: AnyObject {
    init(presenter: NewsInteractorOutputProtocol)
    func fetchNews()
    func getNews(at indexPath: IndexPath)
}

/// Логика презентации
protocol NewsInteractorOutputProtocol: AnyObject {
    func newsDidReceive(_ news: [News])
    func newDidRecive(_ new: News)
}

class NewsInteractor: NewsInteractorInputProtocol {
    
    weak var presenter: NewsInteractorOutputProtocol?
    
    required init(presenter: NewsInteractorOutputProtocol) {
        self.presenter = presenter
    }
    
    func fetchNews() {
        NetworkManager.shared.fetch { news in
            self.presenter?.newsDidReceive(news)
        }
    }
    
    func getNews(at indexPath: IndexPath) {
        let new = DataManager.shared.getNew(at: indexPath)
        presenter?.newDidRecive(new)
    }
}

