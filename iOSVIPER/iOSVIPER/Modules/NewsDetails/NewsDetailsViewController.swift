//
//  NewsDetailsViewController.swift
//  iOSVIPER
//
//  Created by Sergey Stelmakh on 31.10.2021.
//

import UIKit

/// Протокол отображения NewsDetailsViewController-а
protocol NewsDetailsViewInputProtocol: AnyObject {
    func displayUserId(with title: String)
    func displayTitle(with title: String)
}

class NewsDetailsViewController: UIViewController {
    var presenter: NewsDetailsViewOutputProtocol?
    
    
    // MARK: - Labels
    var userIdLabel = UILabel()
    var titleLabel = UILabel()
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.showDetails()
        view.backgroundColor = .white
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        constrains()
    }
}

// MARK: - Setup
extension NewsDetailsViewController {
    fileprivate func setup() {
        view.addSubview(userIdLabel)
        view.addSubview(titleLabel)
    }
}

// MARK: - Constrains
extension NewsDetailsViewController {
    fileprivate func constrains() {
        userIdLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        userIdLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50).isActive = true
        userIdLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 150).isActive = true
        
        
    }
}

// MARK: - NewsDetailsViewInputProtocol
extension NewsDetailsViewController: NewsDetailsViewInputProtocol {
    func displayUserId(with title: String) {
        self.userIdLabel.text = title
    }
    
    func displayTitle(with title: String) {
        self.titleLabel.text = title
    }
}
