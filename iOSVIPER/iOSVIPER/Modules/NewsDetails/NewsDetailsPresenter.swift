//
//  NewsDetailsPresenter.swift
//  iOSVIPER
//
//  Created by Sergey Stelmakh on 31.10.2021.
//

import Foundation

/// Протокол для работы Presenter с NewsDetailViewController
protocol NewsDetailsViewOutputProtocol {
    init(view: NewsDetailsViewInputProtocol)
    func showDetails()
}

class NewsDetailsPresenter: NewsDetailsViewOutputProtocol {
    
    weak var view: NewsDetailsViewInputProtocol?
    var interactor: NewsDetailsInteractorInputProtocol?
    
    required init(view: NewsDetailsViewInputProtocol) {
        self.view = view
    }
    
    func showDetails() {
        interactor?.provideNewsDetails()
    }
}

extension NewsDetailsPresenter: NewsDetailsInteractorOutputProtocol {
    func receiveNewsDetails(with newsDetailsData: NewsDetailsData) {
        let userIdAndId = "UserId - \(newsDetailsData.userId) and id - \(newsDetailsData.id)"
        
        view?.displayUserId(with: userIdAndId)
        view?.displayTitle(with: newsDetailsData.title)
    }
}
