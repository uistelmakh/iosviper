//
//  NewsDetailsInteractor.swift
//  iOSVIPER
//
//  Created by Sergey Stelmakh on 31.10.2021.
//

import Foundation

/// Протокол для работы с NewsInteractor
protocol NewsDetailsInteractorInputProtocol: AnyObject {
    init(presenter: NewsDetailsInteractorOutputProtocol, news: News)
    func provideNewsDetails()
}

/// Логика презентации
protocol NewsDetailsInteractorOutputProtocol: AnyObject {
    func receiveNewsDetails(with newsDetailsData: NewsDetailsData )
}

class NewsDetailsInteractor: NewsDetailsInteractorInputProtocol {
    
    weak var presenter: NewsDetailsInteractorOutputProtocol?
    private let news: News
    
    required init(presenter: NewsDetailsInteractorOutputProtocol, news: News) {
        self.presenter = presenter
        self.news = news
    }
    
    func provideNewsDetails() {
        let newsDetailsData = NewsDetailsData(
            userId: news.userId,
            id: news.id,
            title: news.title
         )
        presenter?.receiveNewsDetails(with: newsDetailsData)
    }
    
    
}
