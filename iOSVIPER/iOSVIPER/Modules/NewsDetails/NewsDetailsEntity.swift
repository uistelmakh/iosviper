//
//  NewsDetailsEntity.swift
//  iOSVIPER
//
//  Created by Sergey Stelmakh on 02.11.2021.
//

import Foundation

struct NewsDetailsData {
    var userId: Int
    var id: Int
    var title: String
//    var body: String
}
