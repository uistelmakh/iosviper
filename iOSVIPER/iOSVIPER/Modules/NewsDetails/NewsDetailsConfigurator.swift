//
//  NewsDetailsConfigurator.swift
//  iOSVIPER
//
//  Created by Sergey Stelmakh on 31.10.2021.
//

import Foundation

protocol NewsDetailsConfiguratorInputProtocol {
    func configure(with viewController: NewsDetailsViewController, news: News)
}

class NewsDetailsConfigurator: NewsDetailsConfiguratorInputProtocol {
    func configure(with viewController: NewsDetailsViewController, news: News) {
        let presenter = NewsDetailsPresenter(view: viewController)
        let interactor = NewsDetailsInteractor(presenter: presenter, news: news)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
    }
}
